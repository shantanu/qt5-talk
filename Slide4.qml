import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Powerful and Rich"
    content: [
        "Qt5 uses C++x011 features",
        "Qt's MOC system makes it possible to do runtime introspection",
        "Signals and Slots mechanism provides hassle free interaction design",
        "Support for event-driven design as well"
    ]
}

import QtQuick 2.0
import Qt.labs.presentation 1.0
import QtGraphicalEffects 1.0

Slide {
    title: "You've already used Qt!"

    ListModel {
        id: qtapps
        ListElement { isVisible: false; name: "Adobe Photoshop Album" }
        ListElement { isVisible: false; name: "Autodesk Maya" }
        ListElement { isVisible: false; name: "Google Earth" }
        ListElement { isVisible: false; name: "KDE Platform and Software" }
        ListElement { isVisible: false; name: "Launchy" }
        ListElement { isVisible: false; name: "MythTV" }
        ListElement { isVisible: false; name: "Skype" }
        ListElement { isVisible: false; name: "VLC Media Player" }
        ListElement { isVisible: false; name: "Oracle VirtualBox" }
    }

    ListView {
        anchors.fill: parent
        model: qtapps
        delegate: Text {
            color: "white"
            text: name
            font.pointSize: 30
            visible: isVisible
        }

        MouseArea {
            property int index: 0
            anchors.fill: parent
            onClicked: qtapps.setProperty(index++, "isVisible", true);
        }
    }
}

import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Open Governance"
    content: [
        "True Free Software project",
        "Governed by meritocracy",
        "You can (and are encouraged to) contribute to the project"
    ]
}

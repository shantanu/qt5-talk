import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Qt Modules - 2"
    content: [
        "QtScript",
        "QtSql",
        "QtSvg",
        "QtWebKit",
        "QtXml",
        "QtXmlPatterns"
    ]
}

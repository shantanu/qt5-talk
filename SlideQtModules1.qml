import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Qt Modules - 1"
    content: [
        "QtCore",
        "QtGui",
        "QtDeclarative",
        "QtMultimedia",
        "QtNetwork",
        "QtOpenGL"
    ]
}

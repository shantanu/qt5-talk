import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "High Performance"
    content: [
        "Build apps using C++",
        "The final result is a native binary for the platform",
        "Platform's API for the final implementation",
        "Qt apps directly use the native widgets and UI elements",
        "QtQuick scenegraph uses OpenGL hardware acceleration",
        "QtQuick JS is powered by V8"
    ]
}

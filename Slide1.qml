import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Cross Platform"
    content: [
        "GNU/Linux, Mac OSX, Windows",
        "MeeGo/Mer, Sailfish, Android (community), iOS (coming soon)",
        "Code less, Create more, Deploy everywhere"
    ]
}

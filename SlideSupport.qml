import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Support"
    content: [
        "As any open source project, developers can refer to community support",
        "Projects can opt for commercial supports from digia Plc and/or its partners ",
        "KDAB in Germany offers Qt support and consulting",
        "VCreateLogic, Bangalore is a KDAB Partner and offers Qt support"
    ]
}

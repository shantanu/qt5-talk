import QtQuick 2.0
import QtGraphicalEffects 1.0

import Qt.labs.presentation 1.0

Item {
    width: 1280
    height: 720

    NoisyGradient {
        anchors.fill: parent;
        gradient: Gradient {
            GradientStop { position: 0.0; color: Qt.rgba(0.64 * 0.6, 0.82 * 0.6, 0.15 * 0.6) }
            GradientStop { position: 1.0; color: "black" }
        }
    }

    OpacityTransitionPresentation {
        id: deck

        anchors.fill: parent

        titleColor: "white"
        textColor: "white"

        layer.enabled: true
        layer.effect: DropShadow {
            horizontalOffset: deck.width * 0.005;
            verticalOffset: deck.width * 0.005;
            radius: 16.0
            samples: 16
            fast: true
            color: Qt.rgba(0.0, 0.0, 0.0, 0.7);
        }

        SlideCounter { }
        Clock { }

        Slide0 {}
        Slide1 {}
        Slide3 {}
        Slide4 {}
        SlideQtInUse {}
        SlideQtModules1 {}
        SlideQtModules2 {}
        SlideQtQuick {}
        SlideDocumentation {}
        SlideSupport {}
        SlideOpenGovernance {}
    }
}

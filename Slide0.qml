import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "Cross Platform Development with Qt"
    Image {
        anchors.centerIn: parent
        source: "qt-logo.png"
    }
}

import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "QtQuick / QML"
    content: [
        "Declarative UI",
        "Ideal for creating modern, fluid user experience"
    ]
}
